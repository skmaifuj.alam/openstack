import requests
import json

def getFlavour(token):
    res = requests.get('http://192.168.56.100:8774/v2.1/flavors',
                    headers={'content-type': 'application/json',
                             'X-Auth-Token':token})
    #print(dir(res))
    #print(res.headers)
    return res.json()
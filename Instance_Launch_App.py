import requests, json
from OStack import get_flavour, get_network, get_token, get_image


class OpenStack:
    token = None
    def __init__(self):
        # self.username = input("Enter Username : ")  # admin
        # self.password = input("Enter Password : ")  # ADMIN_PASS
        self.username = 'admin'
        self.password = 'ADMIN_PASS'

    def _generate_token(self):
        try:
            vv = get_token.getTokens(self.username, self.password)
            print("Login Success:\nToken Generated Below!")
            print(vv['X-Subject-Token'])
            OpenStack.token = vv['X-Subject-Token']
        except KeyError:
            print("Invalid")

    def _generate_image(self, token):
        res_image = get_image.getImage(token)
        print("Images Found Success.")
        print("Image Name   : ", res_image['images'][0]['name'])
        print("Image Id     : ", res_image['images'][0]['id'])

    def generate_flavour(self, token):
        res_flavour = get_flavour.getFlavour(token)
        print("Flavour Found Success.")
        print("Flavour Name :", res_flavour['flavors'][0]['name'])
        print("Flavour Id   : ", res_flavour['flavors'][0]['id'])

    def _generate_network(self, token):
        res_network = get_network.getNetwork(token)
        print("Network Found Success.")
        print("Network Name : ", res_network['networks'][0]['name'])
        print("Network Id   : ", res_network['networks'][0]['id'])


if __name__ == '__main__':
    obj = OpenStack()
    obj._generate_token()
    obj._generate_image(OpenStack.token)
    obj._generate_flavour(OpenStack.token)
    obj._generate_network(OpenStack.token)
    launch = input("Press 1 to Launch Instance or  0 to Exit:")
    if launch == 1:
        instance_name = input("Enter Instance Name:")

import requests
import json

def getImage(token):
    res = requests.get('http://192.168.56.100:9292/v2/images',
                       headers={'content-type': 'application/json',
                                'X-Auth-Token': token
                                },
                       )
    return res.json()
import requests
import json


def getTokens(username="admin", password="ADMIN_PASS"):
    payload = {
        "auth": {
            "identity": {
                "methods": [
                    "password"
                ],
                "password": {
                    "user": {
                        "name": username,
                        "domain": {
                            "name": "Default"
                        },
                        "password": password}
                }
            },
            "scope": {
                "project": {
                    "domain": {
                        "id": "default"
                    },
                    "name": "admin"
                }
            }
        }
    }

    res = requests.post('http://192.168.56.100:5000/v3/auth/tokens',
                        headers={'content-type': 'application/json'},
                        data=json.dumps(payload))

    #print(dir(res))
    print(res.__attrs__)
    print(res.__dict__)
    #print(res.json())  #diff in res.json() and res.headers
    g = str(res.headers)
    print(g)
    return g


getTokens()

import requests
import json


def getTokens(username, password):
    payload = {
        "auth": {
            "identity": {
                "methods": [
                    "password"
                ],
                "password": {
                    "user": {
                        "name": username,
                        "domain": {
                            "name": "Default"
                        },
                        "password": password}
                }
            },
            "scope": {
                "project": {
                    "domain": {
                        "id": "default"
                    },
                    "name": "admin"
                }
            }
        }
    }

    res = requests.post('http://192.168.56.100:5000/v3/auth/tokens',
                        headers={'content-type': 'application/json'},
                        data=json.dumps(payload))
    res1=res.headers
    return res1  #Returns response result into dict
